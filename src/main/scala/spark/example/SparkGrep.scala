package spark.example

import org.apache.spark.SparkContext
import org.apache.spark.SparkConf

object SparkGrep {
	def main(args: Array[String]) {
		val conf = new SparkConf().setAppName("SparkGrep").setMaster("local[*]")
		val sc = new SparkContext(conf)

		val exceptions = Array('"', ',', '.', '!', '-', '(', ')', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '{' ,'}')
		val file = "texts/text.txt"
		val file2 = "texts/text2.txt"

		val firstDictionary =	sc.textFile(file)
				.flatMap(line => line.toLowerCase().filter(symbol => !exceptions.contains(symbol)).split(' '))
				.distinct()

		val seconDictionary = sc.textFile(file2)
			.flatMap(line => line.toLowerCase().filter(symbol => !exceptions.contains(symbol)).split(' '))
			.distinct()

		val wordsDictionary = firstDictionary.sortBy[String](word => word).collect()

		val wordsCounter = sc.textFile(file)
			.map(line => line.toLowerCase().filter(c => !exceptions.contains(c)))
			.map(line => line.split(' ').groupBy(word => word))
			.flatMap(group => group.keys.map(key => (key, group(key).length)))
			.collect()

		val firstDictionaryWithSecondDictionaryWords = firstDictionary.intersection(seconDictionary).sortBy[String](word => word).collect()
		val onlyFirstDictionaryWords = firstDictionary.subtract(seconDictionary).sortBy[String](word => word).collect()
		val onlySecondDictionaryWords = seconDictionary.subtract(firstDictionary).sortBy[String](word => word).collect()


		println("Words dictionary")
		wordsDictionary.foreach(println)

		println
		println("Words counter")
		wordsCounter.foreach(println)

		println
		println("first Dictionary With Second DictionaryWords")
		firstDictionaryWithSecondDictionaryWords.foreach(println)

		println
		println("only First Dictionary Words")
		onlyFirstDictionaryWords.foreach(println)

		println
		println("only Second Dictionary Words")
		onlySecondDictionaryWords.foreach(println)
	}
}
