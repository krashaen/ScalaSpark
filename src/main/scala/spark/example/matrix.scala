package main.scala.spark.example
import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD

/**
  * Created by krashaen on 24.05.17.
  */
object matrix {

  def getFlattenIndexedMatrix(matrix : RDD[Array[Int]]) = {
    matrix
      .map(_.zipWithIndex)
      .zipWithIndex()
      .flatMap(row => row._1.map(cell => (row._2, cell._2, cell._1)))
  }

  def isSymmetric(matrix : RDD[Array[Int]]) : Boolean = {
    val flatten = this.getFlattenIndexedMatrix(matrix)
    val reversed = flatten.map(cell => (cell._2, cell._1, cell._3)).map(cell => cell.toString())

    flatten.map(cell => cell.toString()).subtract(reversed).isEmpty()
  }


  def showMatrix(matrix: Array[Array[Int]]): Unit = {
    matrix.foreach(row => {
      println()
      row.foreach(cell => print("|" + cell + "|"))
    })
    println()
  }


  def multiply(first : RDD[Array[Int]], second : RDD[Array[Int]]) : RDD[Array[Int]] = {
    /* cij = sum(air * brj) */
    val indexedFirst = this.getFlattenIndexedMatrix(first)
    val indexedSecond = this.getFlattenIndexedMatrix(second)

    indexedFirst
      .cartesian(indexedSecond)
      .filter(pair => pair._1._2 == pair._2._1)
      .map(pair => (pair._1._1, pair._2._2 , pair._1._3 * pair._2._3))
      .groupBy(cell => cell._1)
      .map(row => row._2.groupBy(cell => cell._2).values.toArray)
      .map(row => row
        .map(_.toArray)
        .sortBy(_(1))
        .map(_.map(_._3))
    )
      .map(_.map(_.sum))
  }

  def calculate(matrix : RDD[Array[Int]]) : RDD[Array[Int]] = {

    val flatten = this.getFlattenIndexedMatrix(matrix)
    val all = flatten.cartesian(flatten)

    val fourthComponent = all.filter(pair => pair._1._1 == pair._2._1 && pair._1._2 - 2 == pair._2._2)  // get pairs with ai,j-2 and ai,j
    val thirdComponent = all.filter(pair => pair._1._1 == pair._2._1 && pair._1._2 + 2 == pair._2._2)   // get pairs with ai,j+2 and ai,j
    val secondComponent = all.filter(pair => pair._1._1 - 2 == pair._2._1 && pair._1._2 == pair._2._2)  // get pairs with ai-2,j and ai,j
    val firstComponent = all.filter(pair => pair._1._1 + 2 == pair._2._1 && pair._1._2 == pair._2._2)   // get pairs with ai+2,j and ai,j

    Array(firstComponent, secondComponent, thirdComponent, fourthComponent)
      .map(_.map(pair => (pair._2._1.toString + pair._2._2.toString, pair._1, pair._2))) // mark by indexes id this pair
      .reduce(_.union(_))       // unite all pairs
      .groupBy(pair => pair._1) // group all components by new cell indexes
      .map(cell => {
      // calculate according formula (second component and third number)
      val parts = cell._2.toArray
      val value = parts.map(part => part._2._3).sum + parts(1)._3._3 * 4
      (cell._1, value)
    })
      // create new matrix
      .map(cell => {
      val row = cell._1(0)
      val col = cell._1(1)
      (row, col, cell._2)
    })
      .groupBy(cell => cell._1)
      .map(row => row._2.toArray.map(cell => cell._3))
  }



  def main(args: Array[String]): Unit = {

    val conf = new SparkConf().setAppName("SparkGrep").setMaster("local[*]")
    val sc = new SparkContext(conf)

    val matrix = Array(
      Array(2, 2, 1, 2, 2),
      Array(2, 2, 5, 3, 2),
      Array(1, 5, 2, 1, 3),
      Array(2, 3, 1, 2, 3),
      Array(2, 2, 3, 3, 5)

    )

    val matrix2 = Array(
      Array(4, 4, 1, 4, 4, 6),
      Array(4, 4, 5, 3, 4, 7),
      Array(1, 5, 2, 1, 3, 1),
      Array(4, 3, 1, 2, 3, 3),
      Array(4, 4, 3, 3, 5, 9)

    )

    val isSymmetric = this.isSymmetric(sc.parallelize(matrix))
    println("Matrix is %s".format(if (isSymmetric) "symmetric" else "unsymmetric"))

    val multiplied = this.multiply(sc.parallelize(matrix), sc.parallelize(matrix2)).collect()
    println("Multiplied matrix:")
    this.showMatrix(multiplied)

    val calculated = this.calculate(sc.parallelize(matrix)).collect()
    println("\nCalculated matrix:")
    this.showMatrix(calculated)

  }
}

